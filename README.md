# PokeBard

A [React](https://reactjs.org/) & [Next.js](https://nextjs.org/) Pokedex search with a shakespearean twist.

== [Have a preview](https://pokebard.willhowat.vercel.app/)  ==

* Pokemon Data is fetched as JSON from an API   
[https://pokeapi.co/docs/v2](https://pokeapi.co/docs/v2)

* Pokemon content is translated by posting to an API  
[https://funtranslations.com/api/shakespeare](https://funtranslations.com/api/shakespeare)
Note: This API is rate limited to 60 requests per day and 5 per hour. When this limit is reached the application will fall back to un-translated content. 

* A local API is exposed for GET requests 
GET /api/pokemon/<pokemon name>  
Same origin only

## Pre-requisites

You will need the following to get started.  

* [Node](https://nodejs.org/en/) 
* NPM (should come bundled with Node JS)
* Editorconfig extension for VSCode

---

## Getting started

### JS and TS linting settings for ESLint extension

In order for VSCode to correctly work with the linting configuration you will need to add the following to `settings.json`.

(Settings > Workspace tab > Click on any 'Edit in settings.json' link > paste the below with the 'setting' object.)

```
"eslint.validate": [
  "javascript",
  "javascriptreact",
  "typescript",
  "typescriptreact"
]
```

---

## Application spin-up

### Local development server: 
`npm install && npm run dev`  
[http://localhost:3000](http://localhost:3000)

### Local application: 
`npm install && npm run build && npm start`  
[http://localhost:3000](http://localhost:3000)

### Docker
* Start  
`npm run docker-start`  
[http://localhost:5000](http://localhost:5000)
* Refresh  
`npm run docker-refresh`  
[http://localhost:5000](http://localhost:5000)
* Cancel  
`npm run docker-cancel`


---

## Outstanding TODOs

* Add manifest and service worker for PWA support
* Better state management of retrieved pokemon data v.s. locally cached pokedex
* Investigate UI improvements to better indicate when translation has not been possible
