/* eslint-disable max-lines */
import styled, { css } from "styled-components";
import { MouseEvent } from "react";

type ButtonTypes = "button"
	| "submit"
	| "reset";

interface ButtonProps {
	className?: string,
	clickFunc?: (event: MouseEvent<HTMLButtonElement>) => void,
	id?: string,
	isDisabled?: boolean,
	label?: string,
	text?: string,
	type?: ButtonTypes,
}

export default (props: ButtonProps) => {

	const {
		className,
		clickFunc,
		id,
		isDisabled,
		label,
		text,
		type,
	} = props;

	const buttonType: ButtonTypes = type || "button";

	return (
		<Button
			aria-label={label}
			id={id}
			type={buttonType}
			className={className}
			onClick={clickFunc}
			disabled={isDisabled}
		>
			{text}
		</Button>
	);

};

const Button = styled.button`
    flex: 0 0 auto;
	margin: ${(props) => props.theme.base.spacings.base} auto 0 auto;
	padding: ${(props) => props.theme.base.spacings.base} ${(props) => props.theme.base.spacings.positive2};
	background: ${(props) => props.theme.buttonColours.primary};
	color: ${(props) => props.theme.buttonColours.primaryText};
	border: 2px solid ${(props) => props.theme.buttonColours.primaryText};
	text-transform: uppercase;
	cursor: pointer;
	transition: all 0.2s ease-in-out;

	&:hover,
	&:focus,
	&:active {
		color: ${(props) => props.theme.buttonColours.primary};
		background: ${(props) => props.theme.buttonColours.primaryText};
	}

	&[disabled] {
        border-color: ${(props) => props.theme.buttonColours.disabled};
        background-color: ${(props) => props.theme.buttonColours.disabled};
        color: ${(props) => props.theme.buttonColours.disabledText};
    }

	@media screen and (min-width: ${(props) => props.theme.base.breakPoints.small}) {
		margin: 0 0 0 ${(props) => props.theme.base.spacings.positive1};
	}
`;
