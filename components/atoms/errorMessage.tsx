import styled from "styled-components";

interface IErrorMessageProps {
	error?: string,
}

export default (props: IErrorMessageProps) => {

	const {
		error,
	} = props;

	return (
		<>
			{error
				? (
					<ErrorMessage
						className="errorMessage"
					>
						{error}
					</ErrorMessage>
				)
				: null}
		</>
	);

};

const ErrorMessage = styled.p`
    margin: ${(props) => props.theme.base.spacings.negative1} 0 ${(props) => props.theme.base.spacings.positive1};
    font-size: ${(props) => props.theme.base.font.sizes.negative2};
    color: ${(props) => props.theme.textColours.error};
`;
