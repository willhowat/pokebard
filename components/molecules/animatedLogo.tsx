import styled, { css } from "styled-components";
import Logo from "../atoms/logo";

interface IAnimatedLogoProps {
	float?: boolean,
	spin?: boolean,
}

export default (props: IAnimatedLogoProps) => {

	const {
		float,
		spin,
	} = props;

	return (
		<LogoContainer
			float={float}
			spin={spin}
		>
			<Logo />
		</LogoContainer>
	);

};

const LogoContainer = styled.div<IAnimatedLogoProps>`

	@keyframes float {
		0% {
			box-shadow: 0 5px 15px 0px rgba(0,0,0,0.6);
			transform: translatey(0px);
		}
		50% {
			box-shadow: 0 25px 15px 0px rgba(0,0,0,0.2);
			transform: translatey(-20px);
		}
		100% {
			box-shadow: 0 5px 15px 0px rgba(0,0,0,0.6);
			transform: translatey(0px);
		}
	}

	@keyframes spin {
		from {
			transform:rotate(0deg);
			transform-origin: center;
		}
		to {
			transform:rotate(360deg);
			transform-origin: center;
		}
	}

	flex:  0 1 auto;
	display: flex;
	flex-flow: column nowrap;
	align-items: center;
	justify-content: center;
	box-sizing: border-box;
	width: 20vh;
	height: 20vh;
	overflow: hidden;
	object-fit: contain;
	border-radius: 50%;
	transition: all 0.4s ease-in-out;
	box-shadow: 0 5px 15px 0px rgba(0,0,0,0.6);
	transform: translatey(0px);

	${(props) => props.float && css`
		animation: float 3s ease-in-out infinite;
    `};

	${(props) => props.spin && css`
		svg {
			#pokeball {
				animation: spin 1.75s infinite linear;
			}
		}
    `};

`;
