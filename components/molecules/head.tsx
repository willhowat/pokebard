import Head from "next/head";

export default () => (
	<Head>
		<title key="metaTitle">{process.env.NEXT_PUBLIC_APP_NAME}</title>
		<meta key="metaDescription" name="description" content={process.env.NEXT_PUBLIC_APP_DESC} />
		<meta key="metaRobots" name="robots" content="noindex, noimageindex, nofollow, nosnippet, noarchive, nocache" />
		<meta key="metaDnsPrefetchControls" httpEquiv="x-dns-prefetch-control" content="on" />
		<link key="metaDnsPreconnect1" rel="preconnect" href="https://fonts.googleapis.com/" />
		<link key="metaDnsPreconnect8" rel="preconnect" href="https://storage.googleapis.com/" />
		<link key="metaFonts" href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet" />
		<link key="metaFavicon" rel="shortcut icon" href="/favicon.ico" />
		<link key="metaTouchIcon" rel="apple-touch-icon" href="/apple-touch-icon.png" />
		<link key="metaManifest" rel="manifest" href="/manifest.json" crossOrigin="use-credentials" />
	</Head>
);
