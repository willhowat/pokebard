import Button from "../atoms/button";
import { IPokemon } from "../../typeDefs/pokedex";
import styled from "styled-components";

interface IPokemonCardProps {
	pokemon: IPokemon,
	clickFunc?: () => void,
}

export default (props: IPokemonCardProps) => {

	const {
		clickFunc,
		pokemon,
	} = props;

	return (
		<Pokemon>
			<PokemonName>
				{pokemon.name}
			</PokemonName>
			<PokemonDesc>
				{pokemon.description}
			</PokemonDesc>
			<FaveAction>
				<Button
					clickFunc={clickFunc}
					text="Remove Fave"
				/>
			</FaveAction>
		</Pokemon>
	);

};

const Pokemon = styled.li`
	flex: 0 0 auto;
	display: flex;
	flex-flow: column nowrap;
	width: 100%;
	margin: ${(props) => props.theme.base.spacings.positive2} 0 0 0;
	padding: ${(props) => props.theme.base.spacings.positive2};
	background: ${(props) => props.theme.backgrounds.card};
	border: 4px solid ${(props) => props.theme.base.colours.brandPrimary};
	border-radius: ${(props) => props.theme.base.spacings.base};
	box-shadow: 0 1px 0 3px ${(props) => props.theme.buttonColours.disabledText};

	@media screen and (min-width: ${(props) => props.theme.base.breakPoints.small}) {
		width: 45%;
	}
`;

const PokemonName = styled.h2`
	flex: 0 0 auto;
	margin: 0 0 0.9em 0;
	font-size: ${(props) => props.theme.base.font.sizes.positive3};
	color: ${(props) => props.theme.base.colours.brandSecondary};
	text-align: center;
	text-shadow: 0.1em 0.1em 0 ${(props) => props.theme.base.colours.brandPrimary};
	word-wrap: break-word;
`;

const PokemonDesc = styled.p`
	flex: 1 0 auto;
	margin: 0;
	font-size: ${(props) => props.theme.base.font.sizes.negative2};
	text-align: center;
`;

const FaveAction = styled.div`
	flex: 0 0 auto;
	margin-top: ${(props) => props.theme.base.spacings.positive2};
`;
