/* eslint-disable max-len */
import {
	useEffect,
	useState,
} from "react";
import ErrorMessage from "../atoms/errorMessage";
import styled from "styled-components";

interface ITextInputProps {
	autoFocus?: boolean,
	blurFunc?: () => void,
	changeFunc?: (value: string) => void,
	className?: string,
	error?: string,
	focusFunc?: () => void,
	id: string,
	initialValue?: string,
	isDisabled?: boolean,
	label?: string,
	maxLength?: number,
	name: string,
	placeholder?: string,
	text?: string,
	type: string,
	valueOverride?: string,
}

export default (props: ITextInputProps) => {

	const {
		autoFocus,
		blurFunc,
		changeFunc,
		className,
		error,
		focusFunc,
		id,
		initialValue,
		isDisabled,
		label,
		maxLength,
		name,
		placeholder,
		type,
		valueOverride,
	} = props;

	const classToUse = className || "";

	const [
		firstLoad,
		setFirstLoad,
	] = useState(true);

	const [
		value,
		setValue,
	] = useState(initialValue);

	const handleChange = (targetValue: string) => {

		if (changeFunc) {

			changeFunc(targetValue);

		} else {

			setValue(targetValue);

		}

	};

	useEffect(() => {

		setFirstLoad(false);

	}, []);

	if (valueOverride && value !== valueOverride) {

		setValue(valueOverride);

	}

	return (
		<InputWrapper>
			{label
				? (
					<InputLabel
						htmlFor={id}
					>
						{label}
					</InputLabel>
				)
				: null}
			<StyledInput
				autoComplete="off"
				autoFocus={autoFocus}
				className={error && error.length > 0
					? `error ${classToUse}`
					: classToUse}
				disabled={isDisabled}
				id={id}
				maxLength={maxLength}
				name={name}
				onBlur={blurFunc}
				onChange={(event) => handleChange(event.target.value)}
				onFocus={focusFunc}
				placeholder={placeholder}
				type={type}
				value={firstLoad || changeFunc
					? initialValue
					: value}
			/>
			{error
				? (
					<ErrorMessage
						error={error}
					/>
				)
				: null}
		</InputWrapper>
	);

};

export const InputWrapper = styled.div`
	display: flex;
	flex-flow: column wrap;
	width: 100%;
`;

const InputLabel = styled.label`
	position: absolute;
	left: 0;
	top: 0;
	width: 0;
	height: 0;
	overflow: hidden;
`;

export const StyledInput = styled.input`
    display: inline-block;
    width: 100%;
	padding: ${(props) => props.theme.base.spacings.base} ${(props) => props.theme.base.spacings.negative1} ${(props) => props.theme.base.spacings.base} ${(props) => props.theme.base.spacings.positive2};
    font-size: ${(props) => props.theme.base.font.sizes.positive1};
    line-height: ${(props) => props.theme.base.font.sizes.positive1};
	background: ${(props) => props.theme.buttonColours.primaryText};
	color: ${(props) => props.theme.textColours.default};
	border: none;

    &[disabled] {
        border-color: ${(props) => props.theme.buttonColours.disabled};
        background-color: ${(props) => props.theme.buttonColours.disabled};
        color: ${(props) => props.theme.buttonColours.disabledText};
    }
    &[type="search"] {
        appearance: none;
    }
    &[type="search"]::-webkit-search-cancel-button {
        appearance: none;
        width: 0.75em;
        height: 0.75em;
        opacity: 0.6;
        background-position: center;
        background-size: contain;
        background-repeat: no-repeat;
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 90 90'%3E%3Cpath fill='%239B9B9B' d='M45,0 C20.1,0 0,20.1 0,45 C0,69.9 20.1,90 45,90 C69.9,90 90,69.9 90,45 C90,20.1 69.9,0 45,0 Z M58,64 C53.7,59.7 49.3,55.3 45,51 C40.7,55.3 36.3,59.7 32,64 C28.1,67.9 22.2,61.9 26,58 C30.3,53.7 34.7,49.3 39,45 C34.7,40.7 30.3,36.3 26,32 C22.1,28.1 28.1,22.2 32,26 C36.3,30.3 40.7,34.7 45,39 C49.3,34.7 53.7,30.3 58,26 C61.9,22.1 67.8,28.1 64,32 C59.7,36.3 55.3,40.7 51,45 C55.3,49.3 59.7,53.7 64,58 C67.8,61.8 61.8,67.8 58,64 Z'/%3E%3C/svg%3E%0A");
    }
    &.error {
        border-color: ${(props) => props.theme.textColours.error};
    }
`;
