import {
	IPokemon,
	TOGGLE_FAVE,
} from "../../typeDefs/pokedex";
import PokemonCard from "../molecules/pokemonCard";
import styled from "styled-components";
import { useStateValue } from "../../state/state";

export default () => {

	const [
		{
			pokedex,
		},
		dispatch,
	] = useStateValue();

	const handleFaveClick = (pokemonName) => {

		dispatch({
			type: TOGGLE_FAVE,
			value: pokemonName,
		});

	};

	/* eslint-disable react/no-array-index-key */
	return (
		<Faves>
			<List
				aria-live="polite"
			>
				{
					pokedex.pokemon
						.filter((item: IPokemon) => item.isFavourite === true)
						.sort((itemA, itemB) => {

							if (itemA.name > itemB.name) {

								return 1;

							}
							if (itemA.name < itemB.name) {

								return -1;

							}

							return 0;

						})
						.map((fave: IPokemon, index:number) => (
							/* eslint-disable react/no-array-index-key */
							<PokemonCard
								clickFunc={() => handleFaveClick(fave.name)}
								key={`faves-item-${index}`}
								pokemon={fave}
							/>
						))
				}
			</List>
		</Faves>
	);
	/* eslint-enable react/no-array-index-key */

};

const Faves = styled.div`
	grid-area: row2-start / left-side / rows-end / right-side;
	display: flex;
	flex-flow: row wrap;
	align-items: center;
	justify-content: flex-start
`;

const List = styled.ul`
	flex:  1 1 auto;
	display: flex;
	flex-flow: row wrap;
	justify-content: space-around;
	width: 100%;
	margin: ${(props) => props.theme.base.spacings.positive3} auto;
	padding: 0 ${(props) => props.theme.base.spacings.positive2};
	max-width: ${(props) => props.theme.base.spacings.maxContentWidth};
	list-style: none;
`;
