import {
	ADD_TO_POKEDEX,
	IPokemon,
	TOGGLE_FAVE,
} from "../../typeDefs/pokedex";
import {
	GET_POKEMON_WITH_TRANSLATION,
	get,
} from "../../utilities/fetchApi";
import AnimatedLogo from "../molecules/animatedLogo";
import Button from "../atoms/button";
import TextInput from "../molecules/textInput";
import styled from "styled-components";
import { useState } from "react";
import { useStateValue } from "../../state/state";

export default () => {

	const [
		pokemonResponse,
		setPokemonResponse,
	] = useState(null);

	const [
		isSubmitting,
		setIsSubmitting,
	] = useState(false);

	const [
		searchError,
		setSearchError,
	] = useState("");

	const [
		searchValue,
		SetSearchValue,
	] = useState("");

	const [
		{
			pokedex,
		},
		dispatch,
	] = useStateValue();

	const handleSetSearchValue = (value: string) => {

		SetSearchValue(value.trim().toLowerCase());

	};

	const handleFaveClick = () => {

		dispatch({
			type: TOGGLE_FAVE,
			value: pokemonResponse.name,
		});

	};

	const submit = async (event: React.FormEvent<HTMLFormElement>) => {

		event.preventDefault();

		const pokemonName = searchValue;

		if (pokemonName && pokemonName !== "") {

			setIsSubmitting(true);
			setPokemonResponse(null);
			setSearchError("");

			await get(GET_POKEMON_WITH_TRANSLATION, { pokemonName })
				.then((response) => {

					const thisForPokedex: IPokemon = response;
					thisForPokedex.isFavourite = false;

					dispatch({
						type: ADD_TO_POKEDEX,
						value: thisForPokedex,
					});

					setPokemonResponse(thisForPokedex);

				})
				.catch(() => {

					setPokemonResponse(null);
					setSearchError("Alax, this beastie be unknown");

				})
				.finally(() => {

					setIsSubmitting(false);

				});

		}

	};

	return (
		<Search>
			<Content>
				{
					pokemonResponse !== null
						? (
							<Results
								aria-live="polite"
							>
								<PokemonName>
									{
										pokemonResponse && pokemonResponse.name
											? pokemonResponse.name
											: "nope"
									}
								</PokemonName>
								<PokemonDesc>
									{
										pokemonResponse && pokemonResponse.description
											? pokemonResponse.description
											: "nope"
									}
								</PokemonDesc>
								<FaveAction>
									<Button
										clickFunc={handleFaveClick}
										text="Toggle Fave"
									/>
								</FaveAction>
							</Results>
						)
						: (
							<AnimatedLogo
								float
								spin={isSubmitting}
							/>
						)
				}
			</Content>

			<Form method="GET" onSubmit={(event) => submit(event)}>
				<TextInput
					autoFocus
					changeFunc={handleSetSearchValue}
					error={searchError}
					id="searchInputName"
					label="Search for a pokemon by name"
					name="searchInputName"
					placeholder="Search yon beastie"
					type="search"
				/>
				<Button
					isDisabled={isSubmitting}
					text="Catch it"
					type="submit"
				/>
			</Form>
		</Search>
	);

};

const Search = styled.div`
	grid-area: row1-start / left-side / row2-start / right-side;
	display: flex;
	flex-flow: column nowrap;
	align-items: center;
	background: repeating-linear-gradient(
		-45deg,
		#00A0FF,
		#00A0FF 12px,
		#00ABFF 12px,
		#00ABFF 24px
	);
	border-radius: 0 0 50% 50% / 1.5rem;
	box-shadow: 0 6px 0 0 #ffffff;
`;

const Content = styled.div`
	flex: 1 1 auto;
	display: flex;
	flex-flow: column nowrap;
	align-items: center;
	justify-content: center;
	width: 100%;
	margin: ${(props) => props.theme.base.spacings.positive3} auto;
`;

const Results = styled.div`
	flex:  0 0 auto;
	display: flex;
	flex-flow: column nowrap;
	align-items: center;
	padding: 0 ${(props) => props.theme.base.spacings.positive2};
	max-width: ${(props) => props.theme.base.spacings.maxContentWidth};
`;

const PokemonName = styled.h1`
	margin: 0 0 0.7em 0;
	font-size: ${(props) => props.theme.base.font.sizes.positive4};
	color: ${(props) => props.theme.base.colours.brandPrimary};
	text-align: center;
	-webkit-text-stroke-width: 0.1em;
	-webkit-text-stroke-color: ${(props) => props.theme.base.colours.brandTertiary};
	word-break: break-word;
    line-height: 1;
`;

const PokemonDesc = styled.p`
	margin: 0;
	text-align: center;
	color: #ffffff;
`;

const FaveAction = styled.div`
	margin-top: ${(props) => props.theme.base.spacings.positive2};
`;

const Form = styled.form`
	flex: 0 0 auto;
	display: flex;
	flex-flow: column nowrap;
	margin: 0 auto ${(props) => props.theme.base.spacings.positive3} auto;
	padding: 0 ${(props) => props.theme.base.spacings.positive2};
	max-width: ${(props) => props.theme.base.spacings.maxContentWidth};

	input {
		flex: 1 1 auto;
	}

	@media screen and (min-width: ${(props) => props.theme.base.breakPoints.small}) {
        flex-flow: row nowrap;

		button {
			align-self: flex-start;
		}
    }
`;
