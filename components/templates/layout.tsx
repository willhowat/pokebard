import styled,
{ ThemeProvider } from "styled-components";
import { FunctionComponent } from "react";
import GlobalStyles from "../theme/globalStyles";
import LightTheme from "../theme/light";

interface LayoutProps {
	children: FunctionComponent | React.ReactNode | typeof styled,
}

export default (props: LayoutProps) => {

	const {
		children,
	} = props;

	const theme = LightTheme;

	return (
		<ThemeProvider theme={theme}>
			<>
				<GlobalStyles />
				<PageContainer>
					{children}
				</PageContainer>
			</>
		</ThemeProvider>
	);

};

const PageContainer = styled.div`
    display: grid;
	grid:[row1-start] minmax(80vh, min-content) [row2-start] minmax(20vh, min-content) [rows-end]
        / [left-side] 100% [right-side];
	min-height: 100vh;
`;
