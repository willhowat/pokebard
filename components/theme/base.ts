import {
	IBaseTheme,
	IBreakPoints,
	IColours,
	IFontFamilies,
	IFontRules,
	IFontSizes,
	IFontWeights,
	ILayers,
	ILineHeights,
	IResponsiveBaselines,
	ISpacings,
} from "styled-components";

/*
    Media query breakpoint widths:
    default: average large mobile
    small: average tablet portrait
    medium: average tablet landscape
    large: average laptop
    extra_large: average large monitor
*/
const breakPoints: IBreakPoints = {
	default: "480px",
	extraLarge: "1800px",
	large: "1200px",
	medium: "900px",
	small: "600px",
};

const colours: IColours = {
	brandPrimary: "#ffcb05",
	brandSecondary: "#3d7dca",
	brandTertiary: "#003a70",
	pokeBallPrimary: "#ff0000",
	pokeBallSecondary: "#ffffff",
	pokeBallTertiary: "#000000",
};

/*
    Font sizes relative to 16px:
    positive4: 23px
    positive3: 21px
    positive2: 19px
    positive1: 17px
    base: 16px
    negative1: 14px
    negative2: 11px
    negative3: 10px
*/
const sizes: IFontSizes = {
	base: "1rem",
	negative1: "0.875rem",
	negative2: "0.7875rem",
	negative3: "0.625rem",
	positive1: "1.0625rem",
	positive2: "1.1875rem",
	positive3: "1.49rem",
	positive4: "4.4375rem",
};

const weights: IFontWeights = {
	bold: "700",
	regular: "400",
};

const families: IFontFamilies = {
	primary: "'Press Start 2P', cursive",
	secondary: "'Press Start 2P', cursive",
};

const font: IFontRules = {
	families,
	sizes,
	weights,
};

/*
    Z-index layer values
*/
const layers: ILayers = {
	base: 1,
	over: 2,
	overlay: 4,
	sticky: 3,
	under: 0,
};

/*
    Line heights relative to the current font size:
    e.g. base: 16px will be positive1: 24px
*/
const lineHeights: ILineHeights = {
	base: "1em",
	positive1: "1.5em",
};

/*
    Set on html, body in globalStyles
    to provide relative scaling across breakpoints where and if required.
    Provides baseline against which all other REM values are calculated
*/
const responsiveBaselines: IResponsiveBaselines = {
	base: "16px",
};

/*
    Spacings relative to 16px:
    headerHieght: 116px
    positive6: 72px
    positive5: 64px
    positive4: 48px
    positive3: 40px
    positive2: 32px
    positive1: 24px
    base: 16px
    negative1: 8px
*/
const spacings: ISpacings = {
	base: "1rem",
	headerHeight: "7.25rem",
	maxContentWidth: "50rem",
	negative1: "0.5rem",
	positive1: "1.5rem",
	positive2: "2rem",
	positive3: "2.5rem",
	positive4: "3rem",
	positive5: "4rem",
	positive6: "4.5rem",
	positive7: "5rem",
};

const base: IBaseTheme = {
	breakPoints,
	colours,
	font,
	layers,
	lineHeights,
	responsiveBaselines,
	spacings,
};

export default base;
