import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
    * {
        box-sizing: border-box;
    }

    html, body {
        position: relative;
        margin: 0;
        padding: 0;
        min-height: 100%;
        width: 100%;
        font-size: ${(props) => props.theme.base.responsiveBaselines.base};
    }

    body {
        color: ${(props) => props.theme.textColours.default};
        font-family: ${(props) => props.theme.base.font.families.primary};
		background-color: #fffB84;
		background-image:
			linear-gradient(45deg, #fff559 25%, transparent 25%),
			linear-gradient(135deg, #fff559 25%, transparent 25%),
			linear-gradient(45deg, transparent 75%, #fff559 75%),
			linear-gradient(135deg, transparent 75%, #fff559 75%);
			background-size: 31px 31px; /* Must be a square */
			background-position: 0 0, 15.5px 0, 15.5px -15.5px, 0px 15.5px; /* Must be half of one side of the square */
	}

    p {
        margin: 0 0 ${(props) => props.theme.base.spacings.positive1};
        line-height: ${(props) => props.theme.base.spacings.positive1};
    }

    h1,
	h2 {
        margin: 0 0 ${(props) => props.theme.base.spacings.positive1};
        font-size: ${(props) => props.theme.base.font.sizes.positive4};
        font-weight: ${(props) => props.theme.base.font.weights.regular};
        line-height: ${(props) => props.theme.base.spacings.positive2};
        color: ${(props) => props.theme.textColours.default};
    }

	input,
	button {
		font-family: ${(props) => props.theme.base.font.families.primary};
	}

    /* The following stops the auto-fill from Chrome breaking the design */
    input:-webkit-autofill {
        -webkit-box-shadow:0 0 0 50px ${(props) => props.theme.backgrounds.main} inset;
        -webkit-text-fill-color: ${(props) => props.theme.textColours.primary};
    }

    input:-webkit-autofill:focus {
        -webkit-box-shadow: 0 0 0 50px ${(props) => props.theme.backgrounds.main} inset;
        -webkit-text-fill-color: ${(props) => props.theme.textColours.primary};
    }
`;

export default GlobalStyles;
