import {
	DefaultTheme,
	IBackgrounds,
	IButtonColours,
	ITextColours,
} from "styled-components";
import base from "./base";

const backgrounds: IBackgrounds = {
	main: base.colours.pokeBallSecondary,
	card: "#ffffff",
};

const buttonColours: IButtonColours = {
	disabled: "grey",
	disabledText: "darkgrey",
	primary: base.colours.pokeBallPrimary,
	primaryHover: base.colours.pokeBallPrimary,
	primaryText: base.colours.pokeBallSecondary,
	primaryTextHover: base.colours.pokeBallTertiary,
	secondary: base.colours.brandSecondary,
	secondaryHover: base.colours.brandSecondary,
	secondaryText: base.colours.pokeBallSecondary,
	secondaryTextHover: base.colours.pokeBallSecondary,
};

const textColours: ITextColours = {
	attention: base.colours.pokeBallPrimary,
	default: base.colours.pokeBallTertiary,
	error: base.colours.pokeBallPrimary,
	inAttention: base.colours.brandTertiary,
	link: base.colours.pokeBallPrimary,
	primary: base.colours.brandPrimary,
	secondary: base.colours.brandSecondary,
};

const LightTheme: DefaultTheme = {
	backgrounds,
	base,
	buttonColours,
	textColours,
};

export default LightTheme;
