import { AppProps } from "next/app";
import Head from "../components/molecules/head";
import Layout from "../components/templates/layout";
import { StateProvider } from "../state/state";

function MyApp({ Component, pageProps }: AppProps) {

	return (
		<StateProvider>
			<Layout>
				<Head />
				<Component {...pageProps} />
			</Layout>
		</StateProvider>
	);

}

export default MyApp;
