import {
	GET_POKEMON_SPECIES_BY_NAME,
	GET_TRANSLATION,
	get,
	post,
} from "../../../utilities/fetchApi";
import {
	NextApiRequest,
	NextApiResponse,
} from "next";

export default (req: NextApiRequest, res: NextApiResponse) => {

	const {
		query: { name },
	} = req;

	const pokemonName: string = name.toString().trim().toLowerCase();

	const pokemonResponse = {
		description: null,
		isTranslated: false,
		name: null,
	};

	get(GET_POKEMON_SPECIES_BY_NAME, { pokemonName })
		.then((response) => {

			if (response.name) {

				pokemonResponse.name = response.name;

			}

			if (response.flavor_text_entries[0].flavor_text) {

				pokemonResponse.description = response.flavor_text_entries[0].flavor_text;

			}

			if (pokemonResponse.description) {

				post(GET_TRANSLATION, { text: pokemonResponse.description })
					.then((translationResponse) => {

						if (translationResponse) {

							pokemonResponse.description = translationResponse.contents.translated;
							pokemonResponse.isTranslated = true;

						}

						res.statusCode = 200;
						res.json(pokemonResponse);

					})
					.catch(() => {

						if (!pokemonResponse.description) {

							res.statusCode = 404;
							res.end(`Pokemon: ${pokemonName} not translated`);

						} else {

							res.statusCode = 200;
							res.json(pokemonResponse);

						}

					});

			} else {

				res.statusCode = 200;
				res.json(pokemonResponse);

			}

		})
		.catch(() => {

			res.statusCode = 404;
			res.end(`Pokemon: ${pokemonName} not found`);

		});

};
