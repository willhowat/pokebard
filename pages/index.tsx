import Favourites from "../components/organisms/favourites";
import Head from "../components/molecules/head";
import Search from "../components/organisms/search";

export default () => (
	<>
		<Head />
		<Search />
		<Favourites />
	</>
);
