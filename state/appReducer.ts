import {
	AppState,
	initialState,
} from "./initialState";
import {
	clear,
	setStateValue,
} from "../utilities/storage";
import { PokedexActionTypes } from "../typeDefs/pokedex";
import { pokedexReducer } from "./reducers/pokedexReducer";

export const RESET_ALL = "RESET_ALL";

interface IResetAll {
	type: typeof RESET_ALL,
}

export type AppReducerActionTypes = PokedexActionTypes
	| IResetAll;

/* eslint-disable-next-line consistent-return */
export const appReducer = (state: AppState, action: AppReducerActionTypes) => {

	const pokedex = pokedexReducer(state.pokedex, action);

	switch (action.type) {

	case RESET_ALL:

		clear(true);
		return initialState;

	default:
		// Save certain states to storage
		setStateValue("pokedex", pokedex);

		// newState must come first
		return {
			...state,
			...pokedex,
		};

	}

};

export type AppReducerType = ReturnType<typeof appReducer>;
