// Provide only `StateProvider` and `useStateValue` from state folder
import {
	StateContext,
	useStateValue,
} from "./state";

import {
	initialState,
} from "./initialState";

export {
	StateContext,
	useStateValue,
	initialState,
};
