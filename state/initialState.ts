import { getStateValue } from "../utilities/storage";
import { initialState as pokedexState } from "./reducers/pokedexReducer";

export const initialState = {
	...pokedexState,
};

// Add data from storage
const pokedexStorage = getStateValue("pokedex");

export const initialSavedState = {
	...initialState,
	...pokedexStorage,
};

export type AppState = typeof initialState;
