/* eslint-disable no-case-declarations */
import {
	ADD_TO_POKEDEX,
	CLEAR_POKEDEX,
	IPokedex,
	IPokedexValues,
	IPokemon,
	TOGGLE_FAVE,
} from "../../typeDefs/pokedex";
import { AppReducerActionTypes } from "../appReducer";

export const initialState: IPokedex = {
	pokedex: {
		pokemon: [],
	},
};

const checkForDups = (state: IPokedexValues, pokemonName: string) => {

	const thisState = state.pokemon;
	const dup = thisState.find((item: IPokemon) => item.name === pokemonName);

	if (dup) {

		return true;

	}

	return false;

};

const toggleFave = (state: IPokedexValues, pokemonName: string) => {

	const thisState = state.pokemon;
	const pokemonIndex = thisState.findIndex((item: IPokemon) => item.name === pokemonName);
	let faveState = false;

	if (thisState[pokemonIndex].isFavourite !== true) {

		faveState = true;

	}

	return {
		faveState,
		index: pokemonIndex,
	};

};

export const pokedexReducer = (state: IPokedexValues, action: AppReducerActionTypes) => {

	const newState = { ...state };

	switch (action.type) {

	case ADD_TO_POKEDEX:

		if (!checkForDups(newState, action.value.name)) {

			newState.pokemon.push(action.value);

		}
		break;

	case CLEAR_POKEDEX:
		newState.pokemon = [];
		break;

	case TOGGLE_FAVE:

		const targetPokemon = toggleFave(newState, action.value);

		newState.pokemon[targetPokemon.index].isFavourite = targetPokemon.faveState;

		break;

	default:

	}

	return {
		pokedex: newState,
	};

};
