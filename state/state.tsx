// https://medium.com/simply/state-management-with-react-hooks-and-context-api-at-10-lines-of-code-baf6be8302c

import {
	FunctionComponent,
	createContext,
	useContext,
	useReducer,
} from "react";
import { appReducer } from "../state/appReducer";
import { initialSavedState } from "../state/initialState";

/* TD - Find the correct type to remove this `any`
    something like?:
        interface IContextProps {
            state: AppState,
            dispatch: ({type}:{type:string}) => void,
        }
    https://stackoverflow.com/questions/54577865/react-createcontext-issue-in-typescript
*/
const StateContext = createContext({} as any);

// eslint-disable-next-line max-len
const StateProvider: FunctionComponent = ({ children }: any) => (
	<StateContext.Provider value={useReducer(appReducer, initialSavedState)}>
		{children}
	</StateContext.Provider>
);

const useStateValue = () => useContext(StateContext);

export {
	StateContext,
	StateProvider,
	useStateValue,
};
