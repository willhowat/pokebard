export interface IPokemon {
	description: string,
	isFavourite: boolean,
	isTranslated: boolean,
	name: string,
}

export interface IPokedexValues {
    pokemon: Array<IPokemon>,
}

export interface IPokedex {
    pokedex: IPokedexValues,
}

export const ADD_TO_POKEDEX = "ADD_TO_POKEDEX";
export const CLEAR_POKEDEX = "CLEAR_POKEDEX";
export const TOGGLE_FAVE = "TOGGLE_FAVE";

interface IAddToPokedexAction {
    type: typeof ADD_TO_POKEDEX,
    value: IPokemon,
}

interface IClearPokedexAction {
    type: typeof CLEAR_POKEDEX,
}

interface IToggleFaveAction {
    type: typeof TOGGLE_FAVE,
    value: string,
}

export type PokedexActionTypes = IAddToPokedexAction
	| IClearPokedexAction
	| IToggleFaveAction;
