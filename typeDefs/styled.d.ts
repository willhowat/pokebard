import "styled-components";

declare module "styled-components" {

	// Base theme

	export interface IBreakPoints {
        default: string,
        small: string,
        medium: string,
        large: string,
        extraLarge: string,
    }

    export interface IColours {
		brandPrimary: string,
		brandSecondary: string,
		brandTertiary: string,
		pokeBallPrimary: string,
		pokeBallSecondary: string,
		pokeBallTertiary: string,
    }

	export interface IFontFamilies {
        primary: string,
		secondary: string,
	}

	export interface IFontWeights {
        bold: string,
		regular: string,
	}

    export interface IFontRules {
        families: IFontFamilies,
		sizes: IFontSizes,
		weights: IFontWeights,
    }

    export interface IFontSizes {
        base: string,
        negative1: string,
        negative2: string,
        negative3: string,
        positive1: string,
        positive2: string,
        positive3: string,
        positive4: string,
    }

    export interface ILineHeights {
        base: string,
        positive1: string,
    }

    export interface IResponsiveBaselines {
        base: string,
    }

    export interface ISpacings {
        base: string,
        headerHeight: string,
        maxContentWidth: string,
        negative1: string,
        positive1: string,
        positive2: string,
        positive3: string,
        positive4: string,
        positive5: string,
        positive6: string,
        positive7: string,
    }

    export interface ILayers {
        under: number,
        base: number,
        over: number,
        overlay: number,
        sticky: number,
	}

	export interface IBaseTheme {
        breakPoints: IBreakPoints,
        colours: IColours,
        font: IFontRules,
        layers: ILayers,
        lineHeights: ILineHeights,
        spacings: ISpacings,
        responsiveBaselines: IResponsiveBaselines,
	}

	// Interchangable Themes

	export interface IBackgrounds {
		main: string,
		card: string,
    }

    export interface IButtonColours {
		disabled: string,
		disabledText: string,
		primary: string,
		primaryHover: string,
		primaryText: string,
		primaryTextHover: string,
		secondary: string,
		secondaryHover: string,
		secondaryText: string,
		secondaryTextHover: string,
    }

    export interface ITextColours {
        attention: string,
        default: string,
        error: string,
        inAttention: string,
        link: string,
        primary: string,
        secondary: string,
	}

    export interface DefaultTheme {
		backgrounds: IBackgrounds,
		base: IBaseTheme,
		buttonColours: IButtonColours,
		textColours: ITextColours,
    }
}
