/* eslint-disable max-lines */
import fetch from "isomorphic-unfetch";

const RESPONSE_TYPE_JSON = "application/json";

// *
// GET actions
// *

export const GET_POKEMON_SPECIES_BY_NAME = "GET_POKEMON_SPECIES_BY_NAME";
export const GET_POKEMON_WITH_TRANSLATION = "GET_POKEMON_WITH_TRANSLATION";

interface IGetPokemonSpeciesByName {
	pokemonName: string,
}

interface IGetPokemonWithTranslation {
	pokemonName: string,
}

type GetTypes = typeof GET_POKEMON_SPECIES_BY_NAME
	| typeof GET_POKEMON_WITH_TRANSLATION;

type GetQSTypes = IGetPokemonSpeciesByName
	| IGetPokemonWithTranslation;

export const get = async (type: GetTypes, qsValues?: GetQSTypes) => {

	let endPoint = "";
	const responseType = RESPONSE_TYPE_JSON;

	const qsValuesComplete = {
		...qsValues,
	};

	switch (type) {

	case GET_POKEMON_SPECIES_BY_NAME:
		endPoint = `${process.env.NEXT_PUBLIC_POKEMON_API}pokemon-species/${qsValuesComplete.pokemonName}`;
		break;

	case GET_POKEMON_WITH_TRANSLATION:
		endPoint = `/api/pokemon/${qsValuesComplete.pokemonName}`;
		break;

	default:

	}

	const headers = {
		Accept: "application/json",
		"Content-Type": responseType,
	};

	const response = await fetch(
		endPoint,
		{
			headers,
			method: "GET",
		},
	);

	if (response.ok) {

		const responseData = await response.json();

		return responseData;

	}

	const noContent = 404;

	switch (response.status) {

	case noContent:
		return Promise.reject(new Error("404"));

	default:
		return Promise.reject(new Error(response.statusText));

	}

};

// *
// POST actions
// *

export const GET_TRANSLATION = "GET_TRANSLATION";

interface IGetTranslation {
	text: string,
}

type PostTypes = typeof GET_TRANSLATION;

type PostDataTypes = IGetTranslation;

type PostQSTypes = IGetTranslation;

export const post = async (type: PostTypes, data?: PostDataTypes, qsValues?: PostQSTypes) => {

	const body = JSON.stringify(data);
	let endPoint = "";
	const responseType = RESPONSE_TYPE_JSON;

	const qsValuesComplete = {
		...qsValues,
	};

	const headers = {
		Accept: "application/json",
		"Content-Type": responseType,
	};

	switch (type) {

	case GET_TRANSLATION:
		endPoint = `${process.env.NEXT_PUBLIC_TRANSLATION_API}shakespeare.json?text=${qsValuesComplete.text}`;
		break;
	default:

	}

	const response = await fetch(
		endPoint,
		{
			body,
			headers,
			method: "POST",
		},
	);

	const responseData = await response.json();

	if (response.ok) {

		return responseData;

	}

	return Promise.reject(new Error(responseData.message));

};
