const hasStorage = typeof window !== "undefined" &&
	typeof localStorage !== "undefined" &&
	typeof sessionStorage !== "undefined";

const setLocalItem = (key: string, value: string) => {

	if (hasStorage) {

		window.localStorage.setItem(key, value);

	}

};

const getLocalItem = (key: string) => {

	if (hasStorage) {

		return window.localStorage.getItem(key) || "";

	}

	return "";

};

const setSessionItem = (key: string, value: string) => {

	if (hasStorage) {

		window.sessionStorage.setItem(key, value);

	}

};

const getSessionItem = (key: string) => {

	if (hasStorage) {

		return window.sessionStorage.getItem(key) || "";

	}

	return "";

};

export const setStateValue = (name: string, value: Object) => {

	setLocalItem(name, JSON.stringify(value));

};

export const getStateValue = (name: string) => {

	const value = getLocalItem(name);

	if (value && value.length > 0) {

		return JSON.parse(getLocalItem(name));

	}

	return {};

};

export const clear = (clearLocal?: boolean) => {

	if (hasStorage) {

		if (clearLocal) {

			window.localStorage.clear();

		}

		window.sessionStorage.clear();

	}

};

/* eslint-disable */
export const currentUsage = () => {

	const session = window.sessionStorage;
	let data = "";
	let spaceUsed = 0;

	console.log("Current local storage: ");

	for (const key in session) {

		if (session.hasOwnProperty(key)) {

			data += session[key];
			console.log(`${key} = ${(session[key].length * 16 / (8 * 1024)).toFixed(2)} KB`);

		}

	}

	spaceUsed = data.length * 16 / (8 * 1024);

	console.log(data
		? `\nTotal space used: ${spaceUsed.toFixed(2)} KB`
		: "Empty (0 KB)"
	);

	console.log(data
		? `Approx. space remaining: ${(5120 - spaceUsed).toFixed(2)} KB`
		: "5 MB"
	);

};
